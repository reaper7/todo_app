import React from "react";
import "./ToDoListItem.css";
import { Button } from "antd";
import "antd/dist/antd.css";
interface ToDoListItemProps {
  todo: Todo;
  toggleTodoData: ToggleTodo;
  removeToDo: RemoveToDo;
}
export const ToDoListItem: React.FC<ToDoListItemProps> = ({
  todo,
  toggleTodoData,
  removeToDo,
}) => {
  return (
    <li
      style={{
        display: "flex",
        justifyContent: "space-between",
        marginBottom: "10px",
      }}
    >
      <label className={todo.completed ? "complete" : undefined}>
        <input
          style={{ marginRight: "20px" }}
          type="checkbox"
          checked={todo.completed}
          onChange={() => toggleTodoData(todo)}
        />
        {todo.text}
      </label>
      <Button
        style={{ marginLeft: "20px" }}
        type="primary"
        danger
        onClick={() => removeToDo(todo.text)}
      >
        Delete task
      </Button>
    </li>
  );
};
