import react, { ChangeEvent, FormEvent, useState } from "react";
import { Button } from "antd";
import "antd/dist/antd.css";
import { Input, Select } from "antd";

const { Search } = Input;
interface AddToDoFormProps {
  addToDo: AddToDo;
//   removeToDo: RemoveToDo;
}
export const AddToForm: React.FC<AddToDoFormProps> = ({ addToDo }) => {
  const [newTodo, setNewTodo] = useState("");
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setNewTodo(e.target.value);
  };
  //   const handleSubmit = (e: FormEvent<HTMLButtonElement>) => {
  //     e.preventDefault();
  //     addToDo(newTodo);
  //     alert("New Task added");
  //     setNewTodo("");
  //   };
  const handleSubmit = () => {
    addToDo(newTodo);
    if (newTodo !== "") {
      alert("New Task added");
    }
    setNewTodo("");
  };
  return (
    <div>
      <form>
        {/* add a task */}
        <Search
          placeholder="input task name"
          allowClear
          onChange={handleChange}
          value={newTodo}
          enterButton="Add Task"
          size="large"
          onSearch={handleSubmit}
        />
      </form>
    </div>
  );
};
