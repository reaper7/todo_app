import react from "react";
import { ToDoListItem } from "./ToDoListItem";
interface ToDoListProps {
  todos: Array<Todo>;
  toggleTodoData: ToggleTodo;
  removeToDo: RemoveToDo;
}
export const ToDoList: React.FC<ToDoListProps> = ({
  todos,
  toggleTodoData,
  removeToDo,
}) => {
  return (
    <ul style={{ listStyle: "none" }}>
      {todos.map((todo) => {
        return (
          <ToDoListItem
            key={todo.text}
            todo={todo}
            toggleTodoData={toggleTodoData}
            removeToDo={removeToDo}
          />
        );
      })}
    </ul>
  );
};
