type Todo = { text: string; completed: boolean };

type ToggleTodo = (seletedTodo: Todo) => void;

type AddToDo = (newTodo: string) => void;
type RemoveToDo = (slectedTodo: string) => void;
