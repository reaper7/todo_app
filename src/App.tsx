import React, { useState } from "react";
import { ToDoList } from "./ToDoList";
import { AddToForm } from "./AddToDoForm";
const initialTodoes: Array<Todo> = [];

const App: React.FC = () => {
  const [todoes, setTodos] = useState(initialTodoes);
  const toggleTodoe: ToggleTodo = (selectedTodo) => {
    const newTodoes = todoes.map((todo) => {
      if (todo === selectedTodo) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      }
      return todo;
    });
    setTodos(newTodoes);
  };
  // add
  const addToDo: AddToDo = (newTodo) => {
    newTodo.trim() !== "" &&
      setTodos([...todoes, { text: newTodo, completed: false }]);
  };
  // delete
  const removeToDo: RemoveToDo = (slectedTodo) => {
    alert(slectedTodo + " will be deleted. ");
    var todoesNew = todoes;
    console.log(todoesNew.length);
    // search for val to delete
    for (var i = 0; i < todoesNew.length; i++) {
      if (todoesNew[i].text === slectedTodo) {
        todoesNew.splice(i, 1);
      }
    }
    // reset todo list
    setTodos([...todoes]);
  };
  return (
    <React.Fragment>
      <div
        style={{
          marginTop: "20px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <h1 style={{ color: "red" }}>To Do List</h1>
        <br></br>
        <div
          style={{
            display: "flex",
            width: "40%",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          <ToDoList
            todos={todoes}
            toggleTodoData={toggleTodoe}
            removeToDo={removeToDo}
          />
        </div>
        <br></br>
        <AddToForm addToDo={addToDo}></AddToForm>
      </div>
    </React.Fragment>
  );
};

export default App;
